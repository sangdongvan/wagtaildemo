
from wagtail.wagtailcore import blocks
from wagtail.wagtailcore.models import Page
from wagtail.wagtailcore.fields import StreamField
from wagtail.wagtailimages.blocks import ImageChooserBlock
from wagtail.wagtailadmin.edit_handlers import StreamFieldPanel
from wagtail.wagtailsearch import index

from . import blocks as ellenblocks


class LandingPage(Page):
    body = StreamField([
        ('paragraph', blocks.RichTextBlock()),
        ('featured_stories', ellenblocks.FeaturedStoriesBlock(ellenblocks.StoryChooserBlock())),
        ('as_seen_in', ellenblocks.ReferralLinksBlock(blocks.StructBlock([
            ('thumbnail', ImageChooserBlock()),
            ('url', blocks.URLBlock()),
        ]))),
    ], null=True, blank=True)

    search_fields = Page.search_fields + (
        index.SearchField('body'),
    )

LandingPage.content_panels = Page.content_panels + [
    StreamFieldPanel('body'),
]
