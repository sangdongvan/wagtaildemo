
from django.utils.functional import cached_property
from wagtail.wagtailcore import blocks
from wagtail.wagtailimages.blocks import ImageChooserBlock


class StoryChooserBlock(blocks.PageChooserBlock):
    @cached_property
    def target_model(self):
        from demo.models import BlogPage
        return BlogPage


class FeaturedStoriesBlock(blocks.ListBlock):

    class Meta:
        template='ellen/blocks/featured_stories.html'
        icon='cogs'


class ReferralLinksBlock(blocks.ListBlock):

    class Meta:
        template='ellen/blocks/referral_links.html'
        icon='cogs'
