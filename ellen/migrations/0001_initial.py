# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wagtail.wagtailcore.fields
import ellen.blocks
import wagtail.wagtailimages.blocks
import wagtail.wagtailcore.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0019_verbose_names_cleanup'),
    ]

    operations = [
        migrations.CreateModel(
            name='LandingPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, to='wagtailcore.Page', primary_key=True)),
                ('body', wagtail.wagtailcore.fields.StreamField((('paragraph', wagtail.wagtailcore.blocks.RichTextBlock()), ('featured_stories', ellen.blocks.FeaturedStoriesBlock(ellen.blocks.StoryChooserBlock())), ('as_seen_in', ellen.blocks.ReferralLinksBlock(wagtail.wagtailimages.blocks.ImageChooserBlock()))), null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
    ]
