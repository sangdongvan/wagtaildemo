# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models, migrations
import wagtail.wagtailimages.blocks
import wagtail.wagtailcore.fields
import wagtail.wagtailcore.blocks
import ellen.blocks


def convert_referral_links_to_struct_with_url(apps, schema_editor):
    LandingPage = apps.get_model('ellen', 'LandingPage')

    for page in LandingPage.objects.all():
        new_page_body = []

        for child in page.body:
            prep_value = None

            if child.block_type == 'as_seen_in':
                prep_value = [{
                    'thumbnail': image.pk,
                    'url': 'http://example.com',
                } for image in child.value ]
            else:
                prep_value = child.block.get_prep_value(child.value)

            new_page_body.append({
                'type': child.block_type,
                'value': prep_value
            })

        page.body = wagtail.wagtailcore.blocks.StreamValue(page.body.stream_block, [], raw_text=json.dumps(new_page_body, cls=DjangoJSONEncoder))
        page.save()


class Migration(migrations.Migration):

    dependencies = [
        ('ellen', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(
            convert_referral_links_to_struct_with_url,
        ),
        migrations.AlterField(
            model_name='landingpage',
            name='body',
            field=wagtail.wagtailcore.fields.StreamField((('paragraph', wagtail.wagtailcore.blocks.RichTextBlock()), ('featured_stories', ellen.blocks.FeaturedStoriesBlock(ellen.blocks.StoryChooserBlock())), ('as_seen_in', ellen.blocks.ReferralLinksBlock(wagtail.wagtailcore.blocks.StructBlock((('thumbnail', wagtail.wagtailimages.blocks.ImageChooserBlock()), ('url', wagtail.wagtailcore.blocks.URLBlock())))))), blank=True, null=True),
        ),
    ]
