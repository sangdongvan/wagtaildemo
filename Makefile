SHELL=/bin/bash

.PHONY: local install update

local:
	./manage.py runserver 0.0.0.0:8080

update:
	./manage.py migrate
