from .base import *

DEBUG = False
COMPRESS_ENABLED = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'staging_wagtaildemo',
        'USER': 'wagtaildemo',
        'PASSWORD': 'riDSkoEA7WYxgOzljgQaBv3uemjlPLznNUGaRT8uiB8',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

try:
    from .local import *
except ImportError:
    pass
